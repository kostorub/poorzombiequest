﻿using UnityEngine;
using System.Collections;

public class PlayerShadow : MonoBehaviour
{
    private void OnEnable()
    {
        Player.OnPlayerHide += StateChange;     // Когда ГГ соприкоснулся с тригером дерева
    }

    // Use this for initialization
    void Start ()
    {
        gameObject.SetActive( false );
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    /// <summary>
    /// Изменяет Active состояние тени поверх ГГ
    /// </summary>
    /// <param name="state">Состояние ГГ: спрятался / показался</param>
    private void StateChange ( bool state )
    {
        gameObject.SetActive( state );
    }
}
