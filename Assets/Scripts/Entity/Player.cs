﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Класс обработки событий связанных с зомби
/// </summary>
public class Player : MonoBehaviour
{
    private float speed;
    private bool _isHide;
    /// <summary>
    /// Состояние: спрятался / показался </summary>
    public bool IsHide
    {
        get
        {
            return _isHide;
        }

        set
        {
            _isHide = value;
        }
    }
    /// <summary>
    /// Вектор направление движения </summary>
    private Vector3 direction;
    /// <summary>
    /// Направление движения 1 = left, 2 = right, 0 = stop </summary>
    private int     moveSide = 1;
    private IList<KeyCode> keyPressed = new List<KeyCode>();


    public static event EventController.MethodPassBool OnPlayerHide;

    // Use this for initialization
    void Start ()
    {
        GeneralContainer.playerPosition = GetComponent<Transform>().position;
	}

    // Update is called once per frame
    void Update()
    {
        if( keyPressed.Count > 1 )
            speed = 0.014f;
        else if( keyPressed.Count > 2 )
            speed = 0.0f; 
        else
            speed = 0.02f;
        if( Input.GetKey( "w" ) ) { MoveNorth(); }
        if( Input.GetKey( "a" ) ) { MoveWest(); }
        if( Input.GetKey( "s" ) ) { MoveSouth(); }
        if( Input.GetKey( "d" ) ) { MoveEast(); }
        if( Input.GetKeyUp( "w" ) ) { keyPressed.Remove( KeyCode.W ); MoveStop(); } 
        if( Input.GetKeyUp( "a" ) ) { keyPressed.Remove( KeyCode.A ); MoveStop(); } 
        if( Input.GetKeyUp( "s" ) ) { keyPressed.Remove( KeyCode.S ); MoveStop(); } 
        if( Input.GetKeyUp( "d" ) ) { keyPressed.Remove( KeyCode.D ); MoveStop(); }
    }
    private void MoveNorth()
    {
        if( !keyPressed.Contains( KeyCode.W ) )
            keyPressed.Add( KeyCode.W );
        direction = new Vector3( 0, speed, 0 );
        GeneralContainer.playerPosition += direction;
        transform.position += direction;
//        if( OnPlayerMoveNorth != null ) OnPlayerMoveNorth();
        GetComponent<Animator>().SetInteger( "MoveSide", moveSide );
    }
    private void MoveWest()
    {
        if( !keyPressed.Contains( KeyCode.A ) )
            keyPressed.Add( KeyCode.A );
        direction = new Vector3( -speed, 0, 0 );
        GeneralContainer.playerPosition += direction;
        transform.position += direction;
        moveSide = 1;
        GetComponent<Animator>().SetInteger( "MoveSide", moveSide );
    }
    private void MoveSouth()
    {
        if( !keyPressed.Contains( KeyCode.S ) )
            keyPressed.Add( KeyCode.S );
        direction = new Vector3( 0, -speed, 0 );
        GeneralContainer.playerPosition += direction;
        transform.position += direction;
//        if( OnPlayerMoveNorth != null ) OnPlayerMoveSouth();
        GetComponent<Animator>().SetInteger( "MoveSide", moveSide );
    }
    private void MoveEast()
    {
        if( !keyPressed.Contains( KeyCode.D ) )
            keyPressed.Add( KeyCode.D );
        direction = new Vector3( speed, 0, 0 );
        GeneralContainer.playerPosition += direction;
        transform.position += direction;
        moveSide = 2;
        GetComponent<Animator>().SetInteger( "MoveSide", moveSide );
    }
    /// <summary>
    /// Останавливает анимацию движения, если все клавишы отжаты
    /// </summary>
    private void MoveStop()
    {
        if( keyPressed.Count == 0 )
            GetComponent<Animator>().SetInteger( "MoveSide", 0 );
    }

    public void OnTriggerEnter2D( Collider2D enemy )
    {
        switch( enemy.tag )
        {
            case "Tree":
                IsHide = true;
                if ( OnPlayerHide != null )
                    OnPlayerHide( IsHide );
                break;

        }
    }
    public void OnTriggerExit2D( Collider2D enemy )
    {
        switch( enemy.tag )
        {
            case "Tree":
                IsHide = false;
                if( OnPlayerHide != null )
                    OnPlayerHide( IsHide );
                break;
        }
    }
}
