﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Класс для генерации локаций
/// </summary>
public class WorldGenerator: MonoBehaviour
{
    public struct areaTile
    {
        public int type;
        public Vector3 position;
        public int inanimateType;

        public areaTile( int p1, Vector3 p2, int p3 )
        {
            type = p1;
            position = p2;
            inanimateType = p3;
        }
    }

    private areaTile[, ] area;

    private int screenWidth;
    private int screenHeight;
    private int areaWidth = 100;        // Размер игрового поля Ширина
    private int areaHeight = 100;       // Размер игрового поля Высота

    private int tileSize = 64;          // Размер тайла

    private void OnEnable()
    {
 /*       Player.OnPlayerMoveNorth += AreaRegenerationNorth;
        Player.OnPlayerMoveSouth += AreaRegenerationSouth;*/
    }
    private void OnDisable()
    {
/*        Player.OnPlayerMoveNorth -= AreaRegenerationNorth;
        Player.OnPlayerMoveSouth -= AreaRegenerationSouth;*/
    }

    void Start()
    {
        Resolution resolution = Screen.currentResolution;
        screenWidth = resolution.width;
        screenHeight = resolution.height;
        AreaCreate();
        AreaPrepare();
    }

    // Update is called once per frame
    void Update()
    {
    }
    /// <summary>
    /// Генерация поверхности
    /// </summary>
    private void AreaCreate()
    {
        area = new areaTile[ areaHeight, areaWidth ];

        for ( int iC = 0; iC < areaHeight; iC++ )
            for ( int jC = 0; jC < areaWidth; jC++ )
            {
                
                area[ iC, jC ] = new areaTile(
                        UnityEngine.Random.Range( 1, 3 ),
                        new Vector3( jC * tileSize / 100.0f, iC * tileSize / 100.0f, 0 ),
                        GetInanimateTypeId()
                    );
            }
    }
    /// <summary>
    /// Инициализирует матрицу и объекты начальной видимой поверхности
    /// </summary>
    private void AreaPrepare()
    {
        Vector3 tileVector;
        int     tileType;
        string  tileName;
        int     inanimateType;
        string  inanimateName;

        Vector2 delta = GetDeltaTile();

        for ( int iC = 0; iC < areaHeight; iC++ )
            for ( int jC = 0; jC < areaWidth; jC++ )
            {
                tileVector = area[ iC, jC ].position;
                tileType = area[ iC, jC ].type;
                tileName = GetTileType( tileType );

                var tile = Instantiate( Resources.Load( tileName ), tileVector, Quaternion.identity );
                tile.name = tileName + iC.ToString() + "_" + jC.ToString();

                if( area[ iC, jC ].inanimateType == 0 )
                    continue;
                inanimateType = area[ iC, jC ].inanimateType;
                inanimateName = GetInanimateTypeName( inanimateType );

                var inanimate = Instantiate( Resources.Load( inanimateName ), tileVector, Quaternion.identity );
                inanimate.name = inanimateName + iC.ToString() + "_" + jC.ToString();
            }
    }

    /// <summary>
    /// Определяет тип тайла
    /// </summary>
    /// <param name="type">Числовой идентификатор</param>
    /// <returns>Строка, в которой содержится адрес к префабу с тайлом</returns>
    private string GetTileType( int type )
    {
        switch ( type )
        {
            case 1:
                return "Prefabs/World/Ground";
            case 2:
                return "Prefabs/World/Ground1";
        }
        return "Prefabs/World/Ground";
    }

    /// <summary>
    /// Определяет разницу между текущим положением ГГ и нулевой точкой в тайлах
    /// </summary>
    /// <returns>Вектор, содержащий разность по X и по Y</returns>
    private Vector2 GetDeltaTile()
    {
        Vector2 result;
        result.x = (int)System.Math.Truncate( GeneralContainer.playerPosition.x / tileSize * 100.0 );
        result.y = (int)System.Math.Truncate( GeneralContainer.playerPosition.y / tileSize * 100.0 );
        return result;
    }
    /// <summary>
    /// Генератор случайных объектов окружения
    /// </summary>
    /// <returns>Числовой идентификатор типа неодушевленного объекта</returns>
    private int GetInanimateTypeId()
    {
        int r = UnityEngine.Random.Range(0, 100);
        if( r < 5 )
            return 1;
        return 0;
    }
    private string GetInanimateTypeName( int type)
    {
        switch( type )
        {
            case 1:
                return "Prefabs/World/Tree";
            case 2:
                return "Prefabs/World/Shrub";
        }
        return "Prefabs/World/Tree";
    }
    private IEnumerator GeneratorNpc ()
    {
        yield return null;
    }
}
